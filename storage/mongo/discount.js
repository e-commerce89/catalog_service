const { now } = require("mongoose");
const DiscountModel = require("../../models/discount");
const CatchWrapDb = require("../../wrappers/db");

const namespace = "Storage.Discount";

const DiscountStorage = {
    Create: CatchWrapDb(`${namespace}.Create`, async (args) => {
        let discount = new DiscountModel(args);
        const resp = await discount.save();

        resp.discount_id = resp._id;
        return resp;
    }),
    GetByID: CatchWrapDb(`${namespace}.GetByID`, async (args) => {
        if (!args.id) { throw new Error("id is required to get discount") };

        let discount = await DiscountModel.findOne({ _id: args.id });

        if (!discount) { throw new Error("failed to find discount with given id") };

        return discount;
    }),
    GetList: CatchWrapDb(`${namespace}.GetList`, async (args) => {
        let query = {};

        if (args.search.trim()) {
            query = {
                ...query,
                $or: [{
                    'title': { $regex: ".*" + args.search + ".*" },
                },  
                ], // regex here
            }
        };

        let options = {
            limit: args.limit,
            skip: args.offset
        };

        return {
            discounts: await DiscountModel.find(query, {}, options),
            count: await DiscountModel.countDocuments(query)
        };
    }),

    Update: CatchWrapDb(`${namespace}.Update`, async (args) => {
        if (!args.id) { throw new Error("id is required to update discount") }

        let discount = await DiscountModel.findOneAndUpdate(
            {   //query
                _id: args.id,
            },
            {   //update
                $set: {
                    title: args.title,
                    amount:args.amount,
                    type:args.type,
                    status:args.status,
                    updated_at:new Date()
                }
            },
            {   //options
                upsert: false,
                new: true,
            }
        );

        if (!discount) { throw new Error(`discount with given id is not found!`) };

        return discount;
    }),
    Delete: CatchWrapDb(`${namespace}.Delete`, async (args) => {
        if (!args.id) { throw new Error("id is required to delete discount") }
        let discount = await DiscountModel.findOneAndDelete({ _id: args.id });
        if (!discount) { throw new Error(`discount with given id is not found!`) };
        return {};
    }),


};

module.exports = DiscountStorage;
