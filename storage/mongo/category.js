const { now } = require("mongoose");
const CategoryModel = require("../../models/category");
const CatchWrapDb = require("../../wrappers/db");

const namespace = "Storage.Category";

const CategoryStorage = {
    Create: CatchWrapDb(`${namespace}.Create`, async (args) => {
        let category = new CategoryModel(args);
        const resp = await category.save();

        resp.category_id = resp._id;
        return resp;
    }),
    GetByID: CatchWrapDb(`${namespace}.GetByID`, async (args) => {
        if (!args.id) { throw new Error("id is required to get category") };

        let  categories = await CategoryModel.aggregate([
            {
              $match: {
                _id: args.id
              }
            },
            {
              $lookup: {
                from: "categories",
                localField: "_id",
                foreignField: "parent_id",
                as: "category_childs"
              }
            }
          ]);
        if (!categories) { throw new Error("failed to find category with given id") };
        return  categoryResponse = { category: categories[0] };

    }),
    GetList: CatchWrapDb(`${namespace}.GetList`, async (args) => {
        const parentCategories = await CategoryModel.find({
            title: { $regex: args.search, $options: 'i' },
            parent_id: ""
          }).limit(args.limit).skip(args.offset);
          const categoriesWithChilds = await Promise.all(parentCategories.map(async parentCategory => {
            const childCategories = await CategoryModel.find({ parent_id: parentCategory.id });
            parentCategory.category_childs = childCategories;
            return parentCategory;
          }));
          return {
            categories: categoriesWithChilds,
            count: categoriesWithChilds.length
          };
        
        
    }),

    Update: CatchWrapDb(`${namespace}.Update`, async (args) => {
        if (!args.id) { throw new Error("id is required to update category") }

        let category = await CategoryModel.findOneAndUpdate(
            {   //query
                _id: args.id,
            },
            {   //update
                $set: {
                    title: args.title,
                    description: args.description,
                    picture:args.picture,
                    parent_id:args.parent_id,
                    status:args.status,
                    updated_at:new Date()
                }
            },
            {   //options
                upsert: false,
                new: true,
            }
        );

        if (!category) { throw new Error(`category with given id is not found!`) };

        return category;
    }),
    Delete: CatchWrapDb(`${namespace}.Delete`, async (args) => {
        if (!args.id) { throw new Error("id is required to delete category") }
        let category = await CategoryModel.findOneAndDelete({ _id: args.id });
        if (!category) { throw new Error(`category with given id is not found!`) };
        return {};
    }),


};

module.exports = CategoryStorage;
