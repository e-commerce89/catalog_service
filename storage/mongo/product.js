const { now } = require("mongoose");
const ProductModel = require("../../models/product");
const CatchWrapDb = require("../../wrappers/db");


const namespace = "Storage.Product";

const ProductStorage = {
    Create: CatchWrapDb(`${namespace}.Create`, async (args) => {
        let product = new ProductModel(args);
        const resp = await product.save();

        resp.product_id = resp._id;
        return resp;
    }),
    GetByID: CatchWrapDb(`${namespace}.GetByID`, async (args) => {
        if (!args.id) { throw new Error("id is required to get product") };

        let product = await ProductModel.findOne({ _id: args.id }).populate("discount");
        if (!product) { throw new Error("failed to find product with given id") };

        return product;
    }),
    GetList: CatchWrapDb(`${namespace}.GetList`, async (args) => {
      let query = {};
      if (args.search.trim()) {
        query = {
          $or: [
            { title: { $regex: args.search, $options: "i" } }
          ],
        };
      }
      let products = await ProductModel.find(query).populate("discount")
      .limit(args.limit)
      .skip(args.offset)
  
    return {
            products: products,
            count: await ProductModel.countDocuments(query)
        };
    }),

    Update: CatchWrapDb(`${namespace}.Update`, async (args) => {
        if (!args.id) { throw new Error("id is required to update product") }

        let product = await ProductModel.findOneAndUpdate(
            {   //query
                _id: args.id,
            },
            {   //update
                $set: {
                    title: args.title,
                    description: args.description,
                    picture:args.picture,
                    price:args.price,
                    quantity:args.quantity,
                    category_id:args.category_id,
                    status:args.status,
                    discount:args.discount,
                    updated_at:new Date()
                }
            },
            {   //options
                upsert: false,
                new: true,
            }
        );

        if (!product) { throw new Error(`product with given id is not found!`) };

        return product;
    }),
    Delete: CatchWrapDb(`${namespace}.Delete`, async (args) => {
        if (!args.id) { throw new Error("id is required to delete product") }
        let product = await ProductModel.findOneAndDelete({ _id: args.id });
        if (!product) { throw new Error(`product with given id is not found!`) };
        return {};
    }),


};

module.exports = ProductStorage;
