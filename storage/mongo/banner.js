const { now } = require("mongoose");
const BannerModel = require("../../models/banner");
const CatchWrapDb = require("../../wrappers/db");

const namespace = "Storage.Banner";

const BannerStorage = {
    Create: CatchWrapDb(`${namespace}.Create`, async (args) => {
        let banner = new BannerModel(args);
        const resp = await banner.save();

        resp.banner_id = resp._id;
        return resp;
    }),
    GetByID: CatchWrapDb(`${namespace}.GetByID`, async (args) => {
        if (!args.id) { throw new Error("id is required to get banner") };

        let banner = await BannerModel.findOne({ _id: args.id });

        if (!banner) { throw new Error("failed to find banner with given id") };

        return banner;
    }),
    GetList: CatchWrapDb(`${namespace}.GetList`, async (args) => {
        let query = {};

        if (args.search.trim()) {
            query = {
                ...query,
                $or: [{
                    'title': { $regex: ".*" + args.search + ".*" },
                },  
                ], // regex here
            }
        };

        let options = {
            limit: args.limit,
            skip: args.offset
        };

        return {
            banners: await BannerModel.find(query, {}, options),
            count: await BannerModel.countDocuments(query)
        };
    }),

    Update: CatchWrapDb(`${namespace}.Update`, async (args) => {
        if (!args.id) { throw new Error("id is required to update banner") }

        let banner = await BannerModel.findOneAndUpdate(
            {   //query
                _id: args.id,
            },
            {   //update
                $set: {
                    title: args.title,
                    description: args.description,
                    picture:args.picture,
                    discount:args.discount,
                    is_active:args.is_active,
                    url:args.url,
                    updated_at:new Date()
                }
            },
            {   //options
                upsert: false,
                new: true,
            }
        );

        if (!banner) { throw new Error(`banner with given id is not found!`) };

        return banner;
    }),
    Delete: CatchWrapDb(`${namespace}.Delete`, async (args) => {
        if (!args.id) { throw new Error("id is required to delete banner") }
        let banner = await BannerModel.findOneAndDelete({ _id: args.id });
        if (!banner) { throw new Error(`banner with given id is not found!`) };
        return {};
    }),


};

module.exports = BannerStorage;
