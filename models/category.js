var mongoose = require('mongoose');
const { v4 } = require("uuid");

var CategorySchema = new mongoose.Schema(
    {
        _id: {
            type: String,
            default: v4,
        },
        title: {
            type: String,
            required:true
        },
        description: {
            type: String,
            required:true
        },
        picture: {
            type: String,
            required:true
        },
        parent_id: {
            type: String,
            default: ""
        },
        status:{
            type: String,
            default:false
        },
    },
    {
        timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
        toObject: {
            virtuals: true
        },
        toJSON: {
            virtuals: true
        }
    }
);

module.exports = mongoose.model('Category', CategorySchema);