const mongoose = require("mongoose")
const { v4 } = require("uuid")

const BannerSchema = new mongoose.Schema(
    {
        _id: {
            type: String,
            default: v4,
        },
        title: {
            type: String,
            required:true
        },
        description: {
            type: String,
            required:true
        },
        picture: {
            type: String,
            required:true
        },
        url:{
            type:String,
        },
        discount: {
            type: Number,
        },
        is_active: {
            type: Boolean,
        },
    },
    {
        timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
        toObject: {
            virtuals: true
        },
        toJSON: {
            virtuals: true
        }
    }
);


module.exports = mongoose.model("Banner", BannerSchema);
