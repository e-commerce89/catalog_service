const mongoose = require("mongoose")
const { v4 } = require("uuid")

const DiscountSchema = new mongoose.Schema(
    {
        _id: {
            type: String,
            default: v4,
        },
        title: {
            type: String,
        },
        amount: {
            type: Number,
        },
        status: {
            type: Boolean,
        },
        type :{
            type: String,
            enum : ['percentage','fixed'],
            default: 'fixed'
        },
    },
    {
        timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
        toObject: {
            virtuals: true
        },
        toJSON: {
            virtuals: true
        }
    }
);


module.exports = mongoose.model("Discount", DiscountSchema);
