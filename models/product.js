const mongoose = require("mongoose")
const { v4 } = require("uuid")
const Discount = require('./discount');

const ProductSchema = new mongoose.Schema(
    {
        _id: {
            type: String,
            default: v4,
        },
        title: {
            type: String,
            required:true
        },
        description: {
            type: String,
            required:true
        },
        picture: {
            type: String,
            required:true
        },
        price:{
            type:Number,
            required:true
        },
        quantity:{
            type:Number,
            required:true
        },
        category_id:{
            type:String,
            required:true
        },
        discount: { type: String, ref: 'Discount' },
        status: {
            type: Boolean,
        },
    },
    {
        timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
        toObject: {
            virtuals: true
        },
        toJSON: {
            virtuals: true
        }
    }
);


module.exports = mongoose.model("Product", ProductSchema);
