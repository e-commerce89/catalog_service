require('dotenv').config();

const mongoose = require("mongoose");
const protoLoader = require("@grpc/proto-loader");
const grpc = require("@grpc/grpc-js");

const Config = require("./config/index");
const Logger = require("./config/logger");

const CategoryService = require("./services/category");
const BannerService = require('./services/banner');
const ProductService = require('./services/product');
const DiscountService = require('./services/discount');

mongoose.set('strictQuery', true);


// loading proto file
const PROTO_URL = __dirname + "/protos/catalog_service/catalog.proto";
const packageDefinition = protoLoader.loadSync(PROTO_URL, {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true
});

const catalogServiceProto =
    grpc.loadPackageDefinition(packageDefinition).catalog_service;

function main() {
    Logger.debug("Main function is running");

    let mongoDBUrl = `mongodb://${Config.mongoUser}:${Config.mongoPassword}@${Config.mongoHost}:${Config.mongoPort}/${Config.mongoDatabase}`
    if (Config.mongoHost == "localhost") {
        mongoDBUrl = `mongodb://${Config.mongoHost}:${Config.mongoPort}/${Config.mongoDatabase}`
    }
    Logger.debug("Connecting to db: " + mongoDBUrl);

    mongoose.connect(
        mongoDBUrl,
        {
            useNewUrlParser: true,
            useUnifiedTopology: true
        },
        (err) => {
            if (err) {

                console.log(`There is an error in connecting db "${mongoDBUrl}": ${err.message}`)
                Logger.error(
                    `There is an error in connecting db "${mongoDBUrl}": ${err.message}`
                );
                process.exit(0);
            }
        }
    );

    mongoose.connection.once("open", async function () {
        Logger.info("Connected to the database");
    });

    var server = new grpc.Server();

    server.addService(catalogServiceProto.CategoryService.service,CategoryService);  
    server.addService(catalogServiceProto.BannerService.service,BannerService);  
    server.addService(catalogServiceProto.ProductService.service,ProductService); 
    server.addService(catalogServiceProto.DiscountService.service,DiscountService);  
    

    server.bindAsync(
        Config.serviceHost + Config.grpcPort,
        grpc.ServerCredentials.createInsecure(),
        (err, bindPort) => {
            if (err) {
                throw new Error("Error while binding grpc server to the port");
            }

            Logger.info("gRPC server is running at %s", Config.serviceHost + Config.grpcPort);
            
            server.start();
        }
    );
}

main();
