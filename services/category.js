const CategoryStorage = require("../storage/mongo/category");
const CatchWrapService = require("../wrappers/service");

const namespace = "Service.Category";

const CategoryService = {
    Create: CatchWrapService(`${namespace}.Create`, CategoryStorage.Create),
    Update: CatchWrapService(`${namespace}.Update`, CategoryStorage.Update),
    Delete: CatchWrapService(`${namespace}.Delete`, CategoryStorage.Delete),
    GetByID: CatchWrapService(`${namespace}.GetByID`, CategoryStorage.GetByID),
    GetList: CatchWrapService(`${namespace}.GetList`, CategoryStorage.GetList),
};

module.exports = CategoryService;
