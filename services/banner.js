const BannerStorage = require("../storage/mongo/banner");
const CatchWrapService = require("../wrappers/service");

const namespace = "Service.Banner";

const BannerService = {
    Create: CatchWrapService(`${namespace}.Create`, BannerStorage.Create),
    Update: CatchWrapService(`${namespace}.Update`, BannerStorage.Update),
    Delete: CatchWrapService(`${namespace}.Delete`, BannerStorage.Delete),
    GetByID: CatchWrapService(`${namespace}.GetByID`, BannerStorage.GetByID),
    GetList: CatchWrapService(`${namespace}.GetList`, BannerStorage.GetList),
};

module.exports = BannerService;
