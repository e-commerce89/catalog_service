const DiscountStorage = require("../storage/mongo/discount");
const CatchWrapService = require("../wrappers/service");

const namespace = "Service.Discount";

const DiscountService = {
    Create: CatchWrapService(`${namespace}.Create`, DiscountStorage.Create),
    Update: CatchWrapService(`${namespace}.Update`, DiscountStorage.Update),
    Delete: CatchWrapService(`${namespace}.Delete`, DiscountStorage.Delete),
    GetByID: CatchWrapService(`${namespace}.GetByID`, DiscountStorage.GetByID),
    GetList: CatchWrapService(`${namespace}.GetList`, DiscountStorage.GetList),
};

module.exports = DiscountService;