const ProductStorage = require("../storage/mongo/product");
const CatchWrapService = require("../wrappers/service");

const namespace = "Service.Product";

const ProductService = {
    Create: CatchWrapService(`${namespace}.Create`, ProductStorage.Create),
    Update: CatchWrapService(`${namespace}.Update`, ProductStorage.Update),
    Delete: CatchWrapService(`${namespace}.Delete`, ProductStorage.Delete),
    GetByID: CatchWrapService(`${namespace}.GetByID`, ProductStorage.GetByID),
    GetList: CatchWrapService(`${namespace}.GetList`, ProductStorage.GetList),
};

module.exports = ProductService;
